#TOPOLOGY option: torus, foldedclos, hyperX
#ROUTING option: dimension_order, valiants, most_common_ancestor, least_common_ancestor
TOPOLOGY=torus
ROUTING=valiants
ignoreDependency=false
#NAME=lngrex_butterfly
#FILE=lngrex.tra.bz2
#NAME=example_${TOPOLOGY}_${ROUTING}
NAME=example
FILE=example.tra.bz2

if [ -f ./out/${NAME}_messages.mpf ]; then
  echo "./out/${NAME}_messages.mpf already exists!"
  read -r -p "Are you sure you want to overwrite? [y/N] " response
  if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]
  then
    echo "Continue simulation!"
  else
    exit 
  fi
fi

bin/supersim json/${TOPOLOGY}_iq_netrace.json \
  application.trace_file=string=./data/${FILE} \
  network.channel_log.file=string=./out/${NAME}_channels.csv \
  application.rate_log.file=string=./out/${NAME}_rates.csv \
  application.message_log.file=string=./out/${NAME}_messages.mpf \
  network.routing.algorithm=string=${ROUTING} \
  application.ignore_dependencies=bool="${ignoreDependency}"

../sslatency/bin/sslatency -m ./out/${NAME}_messages.csv -p \
  ../out/${NAME}_packets.csv -a ./out/${NAME}_aggregate.csv \
  ./out/${NAME}_messages.mpf

sslqp --title "${NAME} Latency Distribution" ./out/${NAME}_messages.csv \
  ./out/${NAME}_messages.png

python script/trace_parser.py out/ ${NAME}
