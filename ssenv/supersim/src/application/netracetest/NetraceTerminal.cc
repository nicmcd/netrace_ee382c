/*
 * Copyright 2016 Hewlett Packard Enterprise Development LP
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "application/netracetest/NetraceTerminal.h"

#include <cassert>
#include <iostream>

#include "application/netracetest/Application.h"
#include "event/Simulator.h"
#include "types/Message.h"
#include "types/Flit.h"

namespace NetraceTest {

  using std::cout;
  using std::endl;

  /* Event Types */
  const s32 INJECT_MESSAGE_EVENT = 1;

NetraceTerminal::NetraceTerminal(
    const std::string& _name, const Component* _parent, u32 _id,
    const std::vector<u32>& _address, ::Application* _app,
    Json::Value _settings)
    : ::Terminal(_name, _parent, _id, _address, _app) {
}

NetraceTerminal::~NetraceTerminal() {
}

void NetraceTerminal::deliverMessage(Message* _message, u64 _time) {
  u32 packetId = (u32) ((nt_packet_t*) _message->getData())->id;
  u32 dstId = _message->getDestinationId();
  dbgprintf("%d Schedule send message ID: %d SRC: %d DST: %d at time: %d\n", gSim->time(),packetId, getId(), dstId,  _time);
  //_message->print();
  addEvent (_time, 0, _message, INJECT_MESSAGE_EVENT);
}

void NetraceTerminal::processEvent(void* _event, s32 _type) {
  Message * msg = (Message*) _event;
  u32 dstId = msg->getDestinationId();

  u32 packetId = (u32) ((nt_packet_t*) msg->getData())->id;
  dbgprintf("%d Sending message ID: %d SRC: %d DST: %d\n", gSim->time(),packetId, getId(), dstId);
  assert(_type==INJECT_MESSAGE_EVENT);

  sendMessage((Message*)_event, dstId);
}

void NetraceTerminal::handleMessage(Message* _message) {
  //dbgprintf("received message");

  u32 packetId = (u32) ((nt_packet_t*) _message->getData())->id;
  dbgprintf("%d Receiving message ID: %d SRC: %d DST: %d\n", gSim->time(),packetId, _message->getSourceId(), getId());

  // log the message
  Application* app = reinterpret_cast<Application*>(gSim->getApplication());
  app->getMessageLog()->logMessage(_message);

  // end the transaction
  // endTransaction(_message->getTransaction());
  app->ejectMessage(_message);
}

void NetraceTerminal::messageEnteredInterface(Message* _message) {
}

void NetraceTerminal::messageExitedNetwork(Message* _message) {
  ::Terminal::messageExitedNetwork(_message);
  // any override of this function must call the base class's function
}

}  // namespace NetraceTest
