/*
 * Copyright 2016 Hewlett Packard Enterprise Development LP
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "application/netracetest/Application.h"

#include <cassert>

#include <iostream>
#include <vector>
#include <strop/strop.h>
#include "application/netracetest/NetraceTerminal.h"
#include "event/Simulator.h"
#include "network/Network.h"
#include "types/Packet.h"

#define ISPOW2INT(X) (((X) != 0) && !((X) & ((X) - 1)))  /*glibc trick*/
#define ISPOW2(X) (ISPOW2INT(X) == 0 ? false : true)

using std::cout;
using std::endl;
using std::string;


namespace NetraceTest {

  const u32 MEM_CTRL_ID = 9999999;

  /* Event Types */
  const s32 INIT_EVENT = -1;
  const s32 MEM_CTRL_EVENT = 1;

  /* Netrace Packet Types */
  const s32 L1_DATA_TYPE = 0;
  const s32 L1_INSTR_TYPE = 1;
  const s32 L2_TYPE = 2;
  const s32 MEM_CTRL_TYPE = 3;

  /* Terminal Types */
  const s32 L1_DATA_TERMINAL = 2;
  const s32 L1_INSTR_TERMINAL = 1;
  const s32 L2_TERMINAL = 0;

  /* Latency */
  const u32 L2_TAG_LATENCY = 2;
  const u32 L2_DATA_LATENCY = 8;
  const u32 MEM_ACCESS_LATENCY = 150;

Application::Application(const string& _name, const Component* _parent,
                         MetadataHandler* _metadataHandler,
                         Json::Value _settings)
    : ::Application(_name, _parent, _metadataHandler, _settings) {

  numVcs_ = gSim->getNetwork()->numVcs();
  assert(numVcs_ > 0);
  bytesPerFlit_ = _settings["bytes_per_flit"].asUInt();
  assert(bytesPerFlit_ > 0);
  maxPacketSize_ = _settings["max_packet_size"].asUInt();
  // memCtrlLatency represents network latency between L2 and memory
  memCtrlLatency_ = _settings["mem_ctrl_latency"].asUInt();
  ignoreDependencies_ = _settings["ignore_dependencies"].asBool(); 

  string traceFile = _settings["trace_file"].asString();
  // u32 numNetraceTerminals;

  // initialize netrace
  dbgprintf("======== Netrace initialization =========");
  nt_open_trfile( traceFile.c_str() );
  // ignore dependency
  if (ignoreDependencies_) {
    nt_disable_dependencies();
  } else {
    nt_init_self_throttling();
  }
  nt_print_trheader();
  header_ = nt_get_trheader();
  dbgprintf("numNodes:%u\n",(u32) header_->num_nodes);
  // numNetraceTerminals = (u32) header_->num_nodes * 3;
  remainingPacket_ = (u64) header_->num_packets;

  dbgprintf("========= Instantiating Terminals ==========");
  for (u32 t = 0; t < numTerminals(); t++) {
    std::vector<u32> address;
    gSim->getNetwork()->translateIdToAddress(t, &address);
    string idStr = std::to_string(t / 3);
    //dbgprintf("Terminal:%u idStr:%s\n", t, idStr);
    //dbgprintf("%s\n",strop::vecString<u32>(address).c_str());
    string tname;
    if ((t % 3) == L2_TERMINAL) { // L2 terminals
      tname = "L2Terminal_" + idStr;
    } else if ((t % 3) == L1_INSTR_TERMINAL) { // L1 instruction terminals
      tname = "L1InstrTerminal_" + idStr;
    }  else if ((t % 3) == L1_DATA_TERMINAL) { // L1 data terminals
      tname = "L1DataTerminal_" + idStr;
    }
    NetraceTerminal* terminal = new NetraceTerminal(
        tname, this, t, address, this, _settings["NetraceTerminal"]);
    setTerminal(t, terminal);
  }

  // when simulation starts, gSim->startMonitoring();
  addEvent(0, 0, nullptr, INIT_EVENT);

  if (ignoreDependencies_) {
    nt_packet_t* trace_packet = nt_read_packet();
    while(trace_packet != NULL) {
      handlePacket(trace_packet);
      trace_packet = nt_read_packet();
    }
  } else {
    Application::injectMessage();
  }
}

Application::~Application() {}

void Application::injectMessage() {
  //nt_packet_t* trace_packet;
  nt_packet_list_t* list;
  for( list = nt_get_cleared_packets_list(); list != NULL; list = list->next ) {
    if( list->node_packet != NULL ) {
      handlePacket(list->node_packet);
    } else {
      dbgprintf( "Malformed packet list" );
      exit(-1);
    }
  }
  nt_empty_cleared_packets_list();
}

u32 Application::latencyCal(u32 srcType, u32 dstType) {

  u32 latency = 1;
  if (srcType == L2_TYPE && 
    (dstType == L1_INSTR_TYPE || dstType == L1_DATA_TYPE)) {
    latency = L2_DATA_LATENCY;
  } else if (srcType == L2_TYPE && dstType == MEM_CTRL_TYPE) {
    latency = L2_TAG_LATENCY + memCtrlLatency_;
  } else if (srcType == MEM_CTRL_TYPE) {
    latency =  MEM_ACCESS_LATENCY + memCtrlLatency_;
  }

  return latency;
}

void Application::ejectMessage(Message* message) {
  dbgprintf("%d Eject message: \n", gSim->time());
  //nt_print_packet((nt_packet_t*)message->getData());
  nt_clear_dependencies_free_packet((nt_packet_t*)message->getData());
  delete message;
  remainingPacket_--;
  if (remainingPacket_ == 0) {
    dbgprintf("remainingPacket_ = 0");
    nt_close_trfile();
    gSim->endMonitoring();
    dbgprintf(" ---END---");
    return;
  }

  // inject dependency-free messages only if NOT in ignoreDependencies_ mode
  if (!ignoreDependencies_) {
    Application::injectMessage();
  }
}

f64 Application::percentComplete() const {
  u64 tot_packets = (u64) header_->num_packets; 
  f64 pct = (tot_packets - remainingPacket_) / ((f64) tot_packets) ; 
  return pct;
}

u32 Application::numVcs() const {
  return numVcs_;
}

u32 Application::bytesPerFlit() const {
  return bytesPerFlit_;
}

u32 Application::maxPacketSize() const {
  return maxPacketSize_;
}

void Application::processEvent(void* _event, s32 _type) {
  if (_type == INIT_EVENT) {
    dbgprintf("======== NetraceTest Application Starting =======");
    gSim->startMonitoring();
  } else if (_type == MEM_CTRL_EVENT){
    Message* msg = (Message*) _event;
    assert(msg->getDestinationId()==MEM_CTRL_ID ||
        msg->getSourceId()==MEM_CTRL_ID);
    ejectMessage(msg);
  }
}

void Application::handlePacket(nt_packet_t* trace_packet) {
  //trace_packet = list->node_packet;
  
  // determine message length
  int nt_packet_size = nt_get_packet_size(trace_packet);
  assert(nt_packet_size >= 0);
  u32 numFlits = nt_packet_size/bytesPerFlit_;
  u32 numPackets = numFlits / maxPacketSize();
  if ((numFlits % maxPacketSize()) > 0) {
    numPackets++;
  }
  
  // create network message, packets, and flits
  Message* message = new Message(numPackets, trace_packet);
  //message->setTransaction(createTransaction());
  
  u32 flitsLeft = numFlits;
  for (u32 p = 0; p < numPackets; p++) {
    u32 packetLength = flitsLeft > maxPacketSize() ? maxPacketSize() : flitsLeft;
    Packet* packet = new Packet(p, packetLength, message);
    message->setPacket(p, packet);
  
    for (u32 f = 0; f < packetLength; f++) {
      bool headFlit = f == 0;
      bool tailFlit = f == (packetLength - 1);
      Flit* flit = new Flit(f, headFlit, tailFlit, packet);
      packet->setFlit(f, flit);
    }
    flitsLeft -= packetLength;
  }
  
  u32 srcType = (u32) nt_get_src_type(trace_packet);
  u32 dstType = (u32) nt_get_dst_type(trace_packet);
  
  u32 src = trace_packet->src;
  NetraceTerminal* srcTerminal; 
  switch (srcType) {
    case L1_DATA_TYPE: //L1 Data Cache
      {
        srcTerminal = (NetraceTerminal*) getTerminal(src * 3 + L1_DATA_TERMINAL);
        break;
      }
    case L1_INSTR_TYPE: //L1 Instr Cache
      {
        srcTerminal = (NetraceTerminal*) getTerminal(src * 3 + L1_INSTR_TERMINAL);
        break;
      }
    case L2_TYPE: //L2 Cache
      {
        srcTerminal = (NetraceTerminal*) getTerminal(src * 3 + L2_TERMINAL);
        break;
      }
    case MEM_CTRL_TYPE: //Mem Controller
      {
        srcTerminal = nullptr; 
        break;
      }
    default:
      {
        dbgprintf("invalid node type");
        exit(-1);
      }
  }
  
  u32 dst = trace_packet->dst;
  u32 dstId = 0;
  switch (dstType) {
    case 0: //L1 Data Cache
      {
        dstId = dst * 3 + L1_DATA_TERMINAL;
        break;
      }
    case 1: //L1 Instr Cache
      {
        dstId = dst * 3 + L1_INSTR_TERMINAL;
        break;
      }
    case 2: //L2 Cache
      {
        dstId = dst * 3 + L2_TERMINAL;
        break;
      }
    case 3: //Mem Controller
      {
        dstId = MEM_CTRL_ID; 
        break;
      }
    default:
      {
        dbgprintf("invalid node type");
        exit(-1);
      }
  }
  
  // determine the cycle when the message gets sent
  u64 currentCycle = gSim->time() / gSim->cycleTime();
  u32 latency = latencyCal(srcType, dstType);
  u64 cycle;
  if (ignoreDependencies_) {
    cycle = trace_packet->cycle;
  } else {
    cycle = (trace_packet->cycle > (currentCycle + latency)) ?
            trace_packet->cycle : (currentCycle + latency);
  }

  if (dstType==MEM_CTRL_TYPE) {
    message->setDestinationId(MEM_CTRL_ID);
    addEvent (cycle * gSim->cycleTime(), 0, message, MEM_CTRL_EVENT);
  
    u32 packetId = (u32) ((nt_packet_t*) message->getData())->id;
    dbgprintf("%d Schedule Eject Message ID: %d TypeL2->Mem\n", gSim->time(), packetId);
  } else if (srcType==MEM_CTRL_TYPE) {
    message->setSourceId(MEM_CTRL_ID);
    addEvent (cycle * gSim->cycleTime(), 0, message, MEM_CTRL_EVENT);
  
    u32 packetId = (u32) ((nt_packet_t*) message->getData())->id;
    dbgprintf("%d Schedule Eject Message ID: %d Mem->L2\n", gSim->time(), packetId);
  } else {
    message->setDestinationId(dstId);  // Dest Id used by terminal when sendMessage()
    srcTerminal->deliverMessage(message, cycle * gSim->cycleTime());
  }
}

}  // namespace NetraceTest
